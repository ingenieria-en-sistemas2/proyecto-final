
public class Juego {
	private int IdJuego;
	private String Nombre;
	private String Descripcion;
	private int Capacidad;
	private double Precio;
	
	public Juego() {
		
	}

	public Juego(int idJuego, String nombre, String descripcion, int capacidad, double precio) {
		super();
		IdJuego = idJuego;
		Nombre = nombre;
		Descripcion = descripcion;
		Capacidad = capacidad;
		Precio = precio;
	}

	public int getIdJuego() {
		return IdJuego;
	}

	public void setIdJuego(int idJuego) {
		IdJuego = idJuego;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	public int getCapacidad() {
		return Capacidad;
	}

	public void setCapacidad(int capacidad) {
		Capacidad = capacidad;
	}

	public double getPrecio() {
		return Precio;
	}

	public void setPrecio(double precio) {
		Precio = precio;
	}
	
	

}
